Installation
===============
1. Extract the zip/tar.gz file and copy the folder into " Drupal-root/themes/ "
2. Goto example.com/admin/appearance and install and set default the Bootstrap rainbow theme.

Slideshow configuration
========================
You can manage the slideshow on example.com/admin/appearance/settings/rainbow
There is 5 slides defaultly, If you need more just open the rainbow.theme file and change the SLIDESHOW_COUNT value. line no 8

Menu icon configuration
==========================
you can see the rainbow_preprocess_menu() function in rainbow.theme, there is already adding some icon for default menu.
you could do that same thing in the function for your custom menu title.

else if ($menu_link['title'] == 'Contact') {
  $menu_link['title'] =  "<img src='" . THEME_PATH . '/flat-icons/mail.png' . "'  height='60' weight='60' /> " . $menu_link['title'];
}

add this php line in the if condition with change your menu title and icon image.
